﻿using System;

namespace Logger_Console
{
    class Util
    {
        public static bool valid_number(string number, int min, int max)
        {
            int res;
            if ((!Int32.TryParse(number, out res)) || ((min > Int32.Parse(number) || max < Int32.Parse(number))))
            {
                return false;
            }
            return true;
        }

        public static float int_to_float(int i1, int i2, int i3, int i4)
        {
            byte[] floatarray = new byte[4];
            floatarray[0] = (byte)i1;
            floatarray[1] = (byte)i2;
            floatarray[2] = (byte)i3;
            floatarray[3] = (byte)i4;

            return BitConverter.ToSingle(floatarray, 0);
        }

        public static int uin8_to_uin16(int i1, int i2)
        {
            return i1 + 256 * i2;
        }
    }
}
