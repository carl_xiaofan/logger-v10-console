﻿using System;
using System.IO.Ports;

namespace Logger_Console
{
    class Interface
    {
        static void Main(string[] args)
        {
            Menu.Intro();
            Print_ports();
            while (true)
            {
                Console.Write(">");
                Prase_command(Console.ReadLine());
            }
        }
        private static void Prase_command(String command)
        {
            switch (command.Split()[0])
            {
                case "-rescan":
                    Print_ports();
                    break;
                case "-h":
                    Menu.Main_Help();
                    break;
                case "-connect":
                    if (command.Split().Length < 3)
                    {
                        Console.WriteLine("Invalid Command. -c <portname> <Mode>");
                        break;
                    }
                    Connect(command);
                    break;
                default:
                    Console.WriteLine("Command is not proper, enter -h to get more info");
                    break;
            }
        }

        private static bool Connect(string command)
        {
            if (command.Split()[1] == "-usb")
            {
                Connection connection = new Connection(command.Split()[2], 0, 0);
                return connection._connected;
            }
            else if (command.Split()[1] == "-rs232" || command.Split()[1] == "-rs232")
            {
                if (!Util.valid_number(command.Split()[3], 0, 65535))
                {
                    Console.WriteLine("Please enter a valid serial number 0 ~ 65535");
                    return false;
                }
                Connection connection = new Connection(command.Split()[2], 1, Int32.Parse(command.Split()[3]));
                return connection._connected;
            }
            else
            {
                Console.WriteLine("Please enter a valid mode for connection. -usb or -rs232");
            }

            return false;
        }

        private static void Print_ports()
        {
            Console.WriteLine();
            Console.WriteLine("Following are the COM ports currently detected:");
            foreach (String portName in SerialPort.GetPortNames())
            {
                Console.WriteLine(string.Format("- {0}", portName));
            }
        }
    }
}
