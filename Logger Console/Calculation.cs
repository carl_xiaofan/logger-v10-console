﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Logger_Console
{
    class Calculation
    {
        public static float Polynomial(float[] cof, float raw_reading, float offset)
        {
            float result = 0;
            if (offset != 0)
            {
                result += offset;
            }
            else
            {
                result += cof[0];
            }

            if (cof[1] != 0)
            {
                result += cof[1] * raw_reading;
            }
            if (cof[2] != 0)
            {
                result += cof[2] * raw_reading * raw_reading;
            }
            if (cof[3] != 0)
            {
                result += cof[3] * raw_reading * raw_reading * raw_reading;
            }
            if (cof[4] != 0)
            {
                result += cof[4] * raw_reading * raw_reading * raw_reading * raw_reading;
            }

            return result;
        }

        public static double Steinhh(float[] cof, float raw_reading, float offset)
        {
            double result = 0;
            double s = Math.Log(raw_reading);
            if (offset != 0)
            {
                result += offset;
            }
            else
            {
                result += cof[0];
            }
            if (cof[1] != 0)
            {
                result += cof[1] * s;
            }
            if (cof[3] != 0)
            {
                result += cof[3] * s * s * s;
            }

            return result;
        }
    }
}
