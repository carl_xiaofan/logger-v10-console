﻿using Logger_Console;
using System;

namespace Logger_Console
{
    class Packet
    {
        public static Constant.Packet_Info SYN_Packet(int request_serial)
        {
            byte[] packet = new byte[5];
            packet[0] = Constant.START_BYTE;
            packet[1] = (byte)(request_serial & 0x00ff);
            packet[2] = (byte)(request_serial >> 8 & 0x00ff);
            packet[3] = (byte)Constant.Serial_flag.SYN;
            packet[4] = Constant.STOP_BYTE;

            Constant.Packet_Info info = new Constant.Packet_Info(packet, 0, 5);

            return info;
        }

        public static Constant.Packet_Info ACK_SYN_Packet(int request_serial)
        {
            byte[] packet = new byte[5];
            packet[0] = Constant.START_BYTE;
            packet[1] = (byte)(request_serial & 0x00ff);
            packet[2] = (byte)(request_serial >> 8 & 0x00ff);
            packet[3] = (byte)Constant.Serial_flag.ACK_SYN;
            packet[4] = Constant.STOP_BYTE;

            Constant.Packet_Info info = new Constant.Packet_Info(packet, 0, 5);

            return info;
        }

        public static Constant.Packet_Info SET_Serial_Packet(int request_serial, int serial_num)
        {
            byte[] packet = new byte[8];
            packet[0] = Constant.START_BYTE;
            packet[1] = (byte)(request_serial & 0x00ff);
            packet[2] = (byte)(request_serial >> 8 & 0x00ff);
            packet[3] = (byte)Constant.Serial_flag.SET;
            packet[4] = (byte)Constant.Set_flag.SER;
            packet[5] = (byte)(serial_num & 0x00ff);
            packet[6] = (byte)(serial_num >> 8 & 0x00ff);
            packet[7] = Constant.STOP_BYTE;

            Constant.Packet_Info info = new Constant.Packet_Info(packet, 0, 8);

            return info;
        }

        public static Constant.Packet_Info SET_FW_Packet(int request_serial, int serial_num)
        {
            byte[] packet = new byte[8];
            packet[0] = Constant.START_BYTE;
            packet[1] = (byte)(request_serial & 0x00ff);
            packet[2] = (byte)(request_serial >> 8 & 0x00ff);
            packet[3] = (byte)Constant.Serial_flag.SET;
            packet[4] = (byte)Constant.Set_flag.FWV;
            packet[5] = (byte)(serial_num & 0x00ff);
            packet[6] = (byte)(serial_num >> 8 & 0x00ff);
            packet[7] = Constant.STOP_BYTE;

            Constant.Packet_Info info = new Constant.Packet_Info(packet, 0, 8);

            return info;
        }
        
        public static Constant.Packet_Info SET_Time_Packet(int request_serial, int option, int year, int month, int day, int hour, int minute, int second)
        {
            DateTime dt = new DateTime(year, month, day, hour, minute, second);
            byte[] packet = new byte[13];
            packet[0] = Constant.START_BYTE;
            packet[1] = (byte)(request_serial & 0x00ff);
            packet[2] = (byte)(request_serial >> 8 & 0x00ff);
            packet[3] = (byte)Constant.Serial_flag.SET;
            packet[4] = (byte)option;
            packet[5] = (byte)(dt.Year & 0x00ff);
            packet[6] = (byte)(dt.Year >> 8 & 0x00ff);
            packet[7] = (byte)(dt.Month);
            packet[8] = (byte)(dt.Day);
            packet[9] = (byte)(dt.Hour);
            packet[10] = (byte)(dt.Minute);
            packet[11] = (byte)(dt.Second);
            packet[12] = Constant.STOP_BYTE;

            Constant.Packet_Info info = new Constant.Packet_Info(packet, 0, 13);

            return info;
        }

        public static Constant.Packet_Info SET_RTC_Packet(int request_serial)
        {
            DateTime dt = DateTime.Now;
            dt.AddSeconds(2);
            byte[] packet = new byte[13];
            packet[0] = Constant.START_BYTE;
            packet[1] = (byte)(request_serial & 0x00ff);
            packet[2] = (byte)(request_serial >> 8 & 0x00ff);
            packet[3] = (byte)Constant.Serial_flag.SET;
            packet[4] = (byte)Constant.Set_flag.RTC;
            packet[5] = (byte)(dt.Year & 0x00ff);
            packet[6] = (byte)(dt.Year >> 8 & 0x00ff);
            packet[7] = (byte)(dt.Month);
            packet[8] = (byte)(dt.Day);
            packet[9] = (byte)(dt.Hour);
            packet[10] = (byte)(dt.Minute);
            packet[11] = (byte)(dt.Second);
            packet[12] = Constant.STOP_BYTE;

            Constant.Packet_Info info = new Constant.Packet_Info(packet, 0, 13);

            return info;
        }

        public static Constant.Packet_Info SET_Sensor_Type_Packet(int request_serial, int channel, int type)
        {
            byte[] packet = new byte[8];
            packet[0] = Constant.START_BYTE;
            packet[1] = (byte)(request_serial & 0x00ff);
            packet[2] = (byte)(request_serial >> 8 & 0x00ff);
            packet[3] = (byte)Constant.Serial_flag.SET;
            packet[4] = (byte)Constant.Set_flag.SENSOR_TYPE;
            packet[5] = (byte)(channel);
            packet[6] = (byte)(type);
            packet[7] = Constant.STOP_BYTE;

            Constant.Packet_Info info = new Constant.Packet_Info(packet, 0, 8);

            return info;
        }

        public static Constant.Packet_Info SET_VW_Packet(int request_serial, int flag, int channel, int data)
        {
            byte[] packet = new byte[9];
            packet[0] = Constant.START_BYTE;
            packet[1] = (byte)(request_serial & 0x00ff);
            packet[2] = (byte)(request_serial >> 8 & 0x00ff);
            packet[3] = (byte)Constant.Serial_flag.SET;
            packet[4] = (byte)flag;
            packet[5] = (byte)(channel);
            packet[6] = (byte)(data & 0x00ff);
            packet[7] = (byte)(data >> 8 & 0x00ff);
            packet[8] = Constant.STOP_BYTE;

            Constant.Packet_Info info = new Constant.Packet_Info(packet, 0, 9);

            return info;
        }

        public static Constant.Packet_Info SET_Sensor_Equation_Packet(int request_serial, int channel, int equation)
        {
            byte[] packet = new byte[8];
            packet[0] = Constant.START_BYTE;
            packet[1] = (byte)(request_serial & 0x00ff);
            packet[2] = (byte)(request_serial >> 8 & 0x00ff);
            packet[3] = (byte)Constant.Serial_flag.SET;
            packet[4] = (byte)Constant.Set_flag.SENSOR_EQUATION;
            packet[5] = (byte)(channel);
            packet[6] = (byte)(equation);
            packet[7] = Constant.STOP_BYTE;

            Constant.Packet_Info info = new Constant.Packet_Info(packet, 0, 8);

            return info;
        }

        public static Constant.Packet_Info SET_Sensor_COF_Packet(int request_serial, int channel, int cof, float value)
        {
            byte[] f = BitConverter.GetBytes(value);

            byte[] packet = new byte[12];
            packet[0] = Constant.START_BYTE;
            packet[1] = (byte)(request_serial & 0x00ff);
            packet[2] = (byte)(request_serial >> 8 & 0x00ff);
            packet[3] = (byte)Constant.Serial_flag.SET;
            packet[4] = (byte)Constant.Set_flag.SENSOR_COF;
            packet[5] = (byte)(channel);
            packet[6] = (byte)(cof);
            packet[7] = f[0];
            packet[8] = f[1];
            packet[9] = f[2];
            packet[10] = f[3];
            packet[11] = Constant.STOP_BYTE;

            Constant.Packet_Info info = new Constant.Packet_Info(packet, 0, 12);

            return info;
        }

        public static Constant.Packet_Info SET_Interval_Packet(int request_serial, int interval)
        {
            byte[] packet = new byte[8];
            packet[0] = Constant.START_BYTE;
            packet[1] = (byte)(request_serial & 0x00ff);
            packet[2] = (byte)(request_serial >> 8 & 0x00ff);
            packet[3] = (byte)Constant.Serial_flag.SET;
            packet[4] = (byte)Constant.Set_flag.SENSOR_INTERVAL;
            packet[5] = (byte)(interval & 0x00ff);
            packet[6] = (byte)(interval >> 8 & 0x00ff);
            packet[7] = Constant.STOP_BYTE;

            Constant.Packet_Info info = new Constant.Packet_Info(packet, 0, 8);

            return info;
        }

        public static Constant.Packet_Info SET_RESET_Packet(int request_serial)
        {
            byte[] packet = new byte[6];
            packet[0] = Constant.START_BYTE;
            packet[1] = (byte)(request_serial & 0x00ff);
            packet[2] = (byte)(request_serial >> 8 & 0x00ff);
            packet[3] = (byte)Constant.Serial_flag.SET;
            packet[4] = (byte)Constant.Set_flag.RESET_SENSOR;
            packet[5] = Constant.STOP_BYTE;

            Constant.Packet_Info info = new Constant.Packet_Info(packet, 0, 6);

            return info;
        }
       
        public static Constant.Packet_Info SET_RESETSD_Packet(int request_serial)
        {
            byte[] packet = new byte[6];
            packet[0] = Constant.START_BYTE;
            packet[1] = (byte)(request_serial & 0x00ff);
            packet[2] = (byte)(request_serial >> 8 & 0x00ff);
            packet[3] = (byte)Constant.Serial_flag.SET;
            packet[4] = (byte)Constant.Set_flag.RESET_SD;
            packet[5] = Constant.STOP_BYTE;

            Constant.Packet_Info info = new Constant.Packet_Info(packet, 0, 6);

            return info;
        }

        public static Constant.Packet_Info SET_MODE_Packet(int request_serial, int mode)
        {
            byte[] packet = new byte[7];
            packet[0] = Constant.START_BYTE;
            packet[1] = (byte)(request_serial & 0x00ff);
            packet[2] = (byte)(request_serial >> 8 & 0x00ff);
            packet[3] = (byte)Constant.Serial_flag.SET;
            packet[4] = (byte)Constant.Set_flag.MODE;
            packet[5] = (byte)mode;
            packet[6] = Constant.STOP_BYTE;

            Constant.Packet_Info info = new Constant.Packet_Info(packet, 0, 7);

            return info;
        }

        public static Constant.Packet_Info SET_Role_Packet(int request_serial, int role)
        {
            byte[] packet = new byte[7];
            packet[0] = Constant.START_BYTE;
            packet[1] = (byte)(request_serial & 0x00ff);
            packet[2] = (byte)(request_serial >> 8 & 0x00ff);
            packet[3] = (byte)Constant.Serial_flag.SET;
            packet[4] = (byte)Constant.Set_flag.ROLE;
            packet[5] = (byte)role;
            packet[6] = Constant.STOP_BYTE;

            Constant.Packet_Info info = new Constant.Packet_Info(packet, 0, 7);

            return info;
        }

        public static Constant.Packet_Info DAT_RTM_Packet(int request_serial, int rtm)
        {
            byte[] packet = new byte[7];
            packet[0] = Constant.START_BYTE;
            packet[1] = (byte)(request_serial & 0x00ff);
            packet[2] = (byte)(request_serial >> 8 & 0x00ff);
            packet[3] = (byte)Constant.Serial_flag.DAT;
            packet[4] = (byte)Constant.Dat_flag.RTM;
            packet[5] = (byte)(rtm);
            packet[6] = Constant.STOP_BYTE;

            Constant.Packet_Info info = new Constant.Packet_Info(packet, 0, 7);

            return info;
        }

        public static Constant.Packet_Info DAT_RED_Packet(int request_serial, int year, int month, int day)
        {
            byte[] packet = new byte[10];
            packet[0] = Constant.START_BYTE;
            packet[1] = (byte)(request_serial & 0x00ff);
            packet[2] = (byte)(request_serial >> 8 & 0x00ff);
            packet[3] = (byte)Constant.Serial_flag.DAT;
            packet[4] = (byte)Constant.Dat_flag.RED;
            packet[5] = (byte)(year & 0x00ff);
            packet[6] = (byte)(year >> 8 & 0x00ff);
            packet[7] = (byte)month;
            packet[8] = (byte)day;
            packet[9] = Constant.STOP_BYTE;

            Constant.Packet_Info info = new Constant.Packet_Info(packet, 0, 10);

            return info;
        }

        public static Constant.Packet_Info DAT_LST_Packet(int request_serial)
        {
            byte[] packet = new byte[6];
            packet[0] = Constant.START_BYTE;
            packet[1] = (byte)(request_serial & 0x00ff);
            packet[2] = (byte)(request_serial >> 8 & 0x00ff);
            packet[3] = (byte)Constant.Serial_flag.DAT;
            packet[4] = (byte)Constant.Dat_flag.LST;
            packet[5] = Constant.STOP_BYTE;

            Constant.Packet_Info info = new Constant.Packet_Info(packet, 0, 6);

            return info;
        }

        public static Constant.Packet_Info DAT_SensorInfo_Packet(int request_serial, int channel)
        {
            byte[] packet = new byte[7];
            packet[0] = Constant.START_BYTE;
            packet[1] = (byte)(request_serial & 0x00ff);
            packet[2] = (byte)(request_serial >> 8 & 0x00ff);
            packet[3] = (byte)Constant.Serial_flag.DAT;
            packet[4] = (byte)Constant.Dat_flag.SENSOR_INFO;
            packet[5] = (byte)channel;
            packet[6] = Constant.STOP_BYTE;

            Constant.Packet_Info info = new Constant.Packet_Info(packet, 0, 7);

            return info;
        }

        public static Constant.Packet_Info SET_Counter_Reset_Packet(int request_serial, int channel, int reset)
        {
            byte[] packet = new byte[8];
            packet[0] = Constant.START_BYTE;
            packet[1] = (byte)(request_serial & 0x00ff);
            packet[2] = (byte)(request_serial >> 8 & 0x00ff);
            packet[3] = (byte)Constant.Serial_flag.SET;
            packet[4] = (byte)Constant.Set_flag.COUNTER_RESET;
            packet[5] = (byte)(channel);
            packet[6] = (byte)(reset);
            packet[7] = Constant.STOP_BYTE;

            Constant.Packet_Info info = new Constant.Packet_Info(packet, 0, 8);

            return info;
        }
        public static Constant.Packet_Info SET_Sample_Change_Packet(int request_serial, int channel, float value)
        {
            byte[] f = BitConverter.GetBytes(value);

            byte[] packet = new byte[11];
            packet[0] = Constant.START_BYTE;
            packet[1] = (byte)(request_serial & 0x00ff);
            packet[2] = (byte)(request_serial >> 8 & 0x00ff);
            packet[3] = (byte)Constant.Serial_flag.SET;
            packet[4] = (byte)Constant.Set_flag.SAMPLE_CHANGE;
            packet[5] = (byte)(channel);
            packet[6] = f[0];
            packet[7] = f[1];
            packet[8] = f[2];
            packet[9] = f[3];
            packet[10] = Constant.STOP_BYTE;

            Constant.Packet_Info info = new Constant.Packet_Info(packet, 0, 11);

            return info;
        }

        public static Constant.Packet_Info SET_Compensated_Packet(int request_serial, int channel, int enable)
        {
            byte[] packet = new byte[8];
            packet[0] = Constant.START_BYTE;
            packet[1] = (byte)(request_serial & 0x00ff);
            packet[2] = (byte)(request_serial >> 8 & 0x00ff);
            packet[3] = (byte)Constant.Serial_flag.SET;
            packet[4] = (byte)Constant.Set_flag.SENSOR_COMP;
            packet[5] = (byte)(channel);
            packet[6] = (byte)(enable);
            packet[7] = Constant.STOP_BYTE;

            Constant.Packet_Info info = new Constant.Packet_Info(packet, 0, 8);

            return info;
        }

        public static Constant.Packet_Info SET_Compensation_Packet(int request_serial, int channel, int sourec)
        {
            byte[] packet = new byte[8];
            packet[0] = Constant.START_BYTE;
            packet[1] = (byte)(request_serial & 0x00ff);
            packet[2] = (byte)(request_serial >> 8 & 0x00ff);
            packet[3] = (byte)Constant.Serial_flag.SET;
            packet[4] = (byte)Constant.Set_flag.SENSOR_COMPCHAN;
            packet[5] = (byte)(channel);
            packet[6] = (byte)(sourec);
            packet[7] = Constant.STOP_BYTE;

            Constant.Packet_Info info = new Constant.Packet_Info(packet, 0, 8);

            return info;
        }

        public static Constant.Packet_Info SET_Unit_Packet(int request_serial, int channel, string unit)
        {
            byte[] packet = new byte[11];
            packet[0] = Constant.START_BYTE;
            packet[1] = (byte)(request_serial & 0x00ff);
            packet[2] = (byte)(request_serial >> 8 & 0x00ff);
            packet[3] = (byte)Constant.Serial_flag.SET;
            packet[4] = (byte)Constant.Set_flag.SENSOR_UNIT;
            packet[5] = (byte)(channel);
            packet[6] = (byte)(unit[0]);
            packet[7] = (byte)(unit[1]);
            packet[8] = (byte)(unit[2]);
            packet[9] = (byte)(unit[3]);
            packet[10] = Constant.STOP_BYTE;

            Constant.Packet_Info info = new Constant.Packet_Info(packet, 0, 11);

            return info;
        }

        public static Constant.Packet_Info SET_Slave_List_Packet(int request_serial, int index, int serial)
        {
            byte[] packet = new byte[9];
            packet[0] = Constant.START_BYTE;
            packet[1] = (byte)(request_serial & 0x00ff);
            packet[2] = (byte)(request_serial >> 8 & 0x00ff);
            packet[3] = (byte)Constant.Serial_flag.SET;
            packet[4] = (byte)Constant.Set_flag.SLAVE_LIST;
            packet[5] = (byte)(index);
            packet[6] = (byte)(serial & 0x00ff);
            packet[7] = (byte)(serial >> 8 & 0x00ff);
            packet[8] = Constant.STOP_BYTE;

            Constant.Packet_Info info = new Constant.Packet_Info(packet, 0, 9);

            return info;
        }

        public static Constant.Packet_Info DAT_SLST_Packet(int request_serial)
        {
            byte[] packet = new byte[6];
            packet[0] = Constant.START_BYTE;
            packet[1] = (byte)(request_serial & 0x00ff);
            packet[2] = (byte)(request_serial >> 8 & 0x00ff);
            packet[3] = (byte)Constant.Serial_flag.DAT;
            packet[4] = (byte)Constant.Dat_flag.SLST;
            packet[5] = Constant.STOP_BYTE;

            Constant.Packet_Info info = new Constant.Packet_Info(packet, 0, 6);

            return info;
        }

        public static Constant.Packet_Info SET_Slave_Number_Packet(int request_serial, int slave_number)
        {
            byte[] packet = new byte[7];
            packet[0] = Constant.START_BYTE;
            packet[1] = (byte)(request_serial & 0x00ff);
            packet[2] = (byte)(request_serial >> 8 & 0x00ff);
            packet[3] = (byte)Constant.Serial_flag.SET;
            packet[4] = (byte)Constant.Set_flag.SLAVE_NUMBER;
            packet[5] = (byte)slave_number;
            packet[6] = Constant.STOP_BYTE;

            Constant.Packet_Info info = new Constant.Packet_Info(packet, 0, 7);

            return info;
        }
    }
}
