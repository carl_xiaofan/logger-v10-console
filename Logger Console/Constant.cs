﻿using System;

namespace Logger_Console
{
    class Constant
    {
        public static byte START_BYTE = 0xB2; //10101010
        public static byte STOP_BYTE = 0xDC;  //01010101

        public static byte[] START_BYTE_ARY = new byte[] { START_BYTE };
        public static byte[] STOP_BYTE_ARY = new byte[] { STOP_BYTE };

        public static string[] sensor_types = new string[6]{"Unused", "Vibrating Wire", "Resistance", "Voltage", "Frequency", "Counter"};
        public static string[] equation_types = new string[3] { "Raw Data", "Polynomial", "SteinhartHart"};

        public enum Serial_flag
        {
            FIN = 0x01,
            SYN = 0x02,
            ACK = 0x04,
            ACK_SYN = 0x06,
            SET = 0x08,
            DAT = 0x10,
            FOR = 0x20,
        };

        public enum Set_flag
        {
            SER = 0x01,
            FWV = 0x02,
            RTC = 0x03,
            BUILD_DATE = 0x04,
            CHECK_DATE = 0x05,
            MODE = 0x06,
            ROLE = 0x07,
            SENSOR_TYPE = 0X08,
            SENESOR_SETTLE = 0X09,
            SENSOR_START = 0X0A,
            SENSOR_STOP = 0x0B,
            SENSOR_CYCLES = 0x0C,
            SENSOR_INTERVAL = 0x0D,
            SENSOR_EQUATION = 0x0E,
            SENSOR_COF = 0x0F,
            SENSOR_COMP = 0x10,
            SENSOR_COMPCHAN = 0x11,
            COUNTER_RESET = 0x12,
            SAMPLE_CHANGE = 0x13,
            SENSOR_UNIT = 0x14,
            RESET_SENSOR = 0x15,
            RESET_SD = 0x16,
            SLAVE_LIST = 0x17,
            SLAVE_NUMBER = 0x18,
        };

        public enum Dat_flag
        {
            RTM = 0x01,
            RED = 0x02,
            LST = 0x03,
            SENSOR_INFO = 0x04,
            SLST = 0x05,
        };

        public struct Packet_Info
        {
            public byte[] data;
            public int offset, count;

            public Packet_Info(byte[] d, int o, int c)
            {
                data = d;
                offset = o;
                count = c;
            }
        }

        public struct Record
        {
            public DateTime t;
            public float[] readings;

            public Record(DateTime record_time, float[] record_readings)
            {
                t = record_time;
                readings = record_readings;
            }
        }

        public struct Logger_Info
        {
            public int serial_num;
            public int fmw_num;
            public int role;
            public DateTime last_set;
            public DateTime build_date;
            public DateTime check_point;
            public Sensor_Info[] sensor_info;
            public int[] slave_list;

            public Logger_Info(int serial = 0)
            {
                serial_num = serial;
                fmw_num = 0;
                role = 0;
                last_set = new DateTime();
                build_date = new DateTime();
                check_point = new DateTime();
                sensor_info = new Sensor_Info[8];
                slave_list = new int[5];
            }
        }

        public struct Sensor_Info
        {
            public int type;
            public int settle;
            public int excite_start;
            public int excite_stop;
            public int cycle;
            public int equation;
            public string unit;
            public float[] cof;
            public int compensation;
            public float record_change;
            public int counter_reset;

            public Sensor_Info(int t = 0)
            {
                type = 0;
                settle = 0;
                excite_start = 0;
                excite_stop = 0;
                cycle = 0;
                equation = 0;
                unit = "";
                compensation = 0;
                cof = new float[5];
                record_change = 0;
                counter_reset = 0;
            }
        }
    }
}
