﻿using System;
using System.IO;
using System.IO.Ports;
using System.Threading;
using Logger_Console;

namespace Logger_Console
{
    class Connection
    {

        public bool _connected = false;

        private String _portName = null;

        private SerialPort _serialPort = null;

        public int request_serial = 0;

        public Constant.Logger_Info logger_info = new Constant.Logger_Info();

        private int[] got_channel = new int[8] { 0, 0, 0, 0, 0, 0, 0, 0 };

        public Connection(String portName, int mode, int serial_num)
        {
            _portName = portName;

            request_serial = serial_num;

            if (Establish_connection(mode))
            {
                _connected = true;
                Console.WriteLine("Enter connection mode, enter -h to get more info");

                while (true)
                {
                    Console.Write(">");
                    if (!Prase_command(Console.ReadLine()))
                    {
                        break;
                    }
                }
            }
        }
        private bool Prase_command(String command)
        {
            switch (command.Split()[0])
            {
                case "-h":
                    Menu.Connected_Help();
                    break;
                case "-disconnect":
                    Disconnect();
                    return false;
                case "-set":
                    Push_setting(command);
                    break;
                case "-seth":
                    Menu.Setting_Help();
                    break;
                case "-geth":
                    Menu.Get_Help();
                    break;
                case "-rtm":
                    SendPacket(Packet.DAT_RTM_Packet(request_serial, 1));
                    RTM_mode();
                    break;
                case "-get":
                    Get_data(command);
                    break;
                case "-clear":
                    Console.Clear();
                    break;
                default:
                    Console.WriteLine("Command is not proper, enter -h to show command dictionary");
                    break;
            }

            return true;
        }

        private bool Establish_connection(int mode)
        {
            try
            {
                if (mode == 0)
                {
                    _serialPort = new SerialPort(_portName, 9600, Parity.None, 8, StopBits.One);
                }
                else
                {
                    _serialPort = new SerialPort(_portName, 115200, Parity.None, 8, StopBits.One);
                }

                _serialPort.Handshake = Handshake.None;

                _serialPort.Open();
            }
            catch (Exception)
            {
                Console.WriteLine("Open Port Failed");
                return false;
            }

            if (!Start_handshake())
            {
                _serialPort.Close();
                return false;
            }

            return true;
        }

        private bool Push_setting(String command)
        {
            switch (command.Split()[1])
            {
                case "serial_number":
                    if (command.Split().Length != 3)
                    {
                        Console.WriteLine("Can not prase command. -set serialnumber <serial number>");
                        break;
                    }
                    if (!Util.valid_number(command.Split()[2], 0, 65535))
                    {
                        Console.WriteLine("Please enter a valid serial number between 0 ~ 65535");
                        break;
                    }
                    SendPacket(Packet.SET_Serial_Packet(request_serial, Int32.Parse(command.Split()[2])));
                    break;

                case "firmware_version":
                    if (command.Split().Length != 3)
                    {
                        Console.WriteLine("Can not prase command. -set firmwareversion <firmware version>");
                        break;
                    }
                    if (!Util.valid_number(command.Split()[2], 0, 999))
                    {
                        Console.WriteLine("Please enter a valid firmware version number between 0 ~ 999");
                        break;
                    }
                    SendPacket(Packet.SET_FW_Packet(request_serial, Int32.Parse(command.Split()[2])));
                    break;

                case "clock":
                    if (command.Split().Length == 8)
                    {
                        if (Util.valid_number(command.Split()[2], 0, 9999) && Util.valid_number(command.Split()[3], 0, 12) && 
                            Util.valid_number(command.Split()[4], 0, 31) && Util.valid_number(command.Split()[5], 0, 24) && 
                            Util.valid_number(command.Split()[6], 0, 60) && Util.valid_number(command.Split()[7], 0, 60))
                        {
                            SendPacket(Packet.SET_Time_Packet(request_serial, (int)Constant.Set_flag.RTC, Int32.Parse(command.Split()[2]),
                            Int32.Parse(command.Split()[3]), Int32.Parse(command.Split()[4]),
                            Int32.Parse(command.Split()[5]), Int32.Parse(command.Split()[6]),
                            Int32.Parse(command.Split()[7])));
                        }
                        else
                        {
                            Console.WriteLine("Please enter a valid date");
                        }
                    }
                    else if (command.Split().Length == 2)
                    {
                        SendPacket(Packet.SET_RTC_Packet(request_serial));
                    }
                    else
                    {
                        Console.WriteLine("Can not prase command.");
                    }
                    break;

                case "build_date":
                    if (command.Split().Length == 8)
                    {
                        if (Util.valid_number(command.Split()[2], 0, 9999) && Util.valid_number(command.Split()[3], 0, 12) &&
                            Util.valid_number(command.Split()[4], 0, 31) && Util.valid_number(command.Split()[5], 0, 24) &&
                            Util.valid_number(command.Split()[6], 0, 60) && Util.valid_number(command.Split()[7], 0, 60))
                        {
                            SendPacket(Packet.SET_Time_Packet(request_serial, (int)Constant.Set_flag.BUILD_DATE, Int32.Parse(command.Split()[2]),
                            Int32.Parse(command.Split()[3]), Int32.Parse(command.Split()[4]),
                            Int32.Parse(command.Split()[5]), Int32.Parse(command.Split()[6]),
                            Int32.Parse(command.Split()[7])));
                        }
                        else
                        {
                            Console.WriteLine("Please enter a valid date");
                        }
                    }
                    else
                    {
                        Console.WriteLine("Can not prase command.");
                    }
                    break;

                case "check_point":
                    if (command.Split().Length == 8)
                    {
                        if (Util.valid_number(command.Split()[2], 0, 9999) && Util.valid_number(command.Split()[3], 0, 12) &&
                            Util.valid_number(command.Split()[4], 0, 31) && Util.valid_number(command.Split()[5], 0, 24) &&
                            Util.valid_number(command.Split()[6], 0, 60) && Util.valid_number(command.Split()[7], 0, 60))
                        {
                            SendPacket(Packet.SET_Time_Packet(request_serial, (int)Constant.Set_flag.CHECK_DATE, Int32.Parse(command.Split()[2]),
                            Int32.Parse(command.Split()[3]), Int32.Parse(command.Split()[4]),
                            Int32.Parse(command.Split()[5]), Int32.Parse(command.Split()[6]),
                            Int32.Parse(command.Split()[7])));
                        }
                        else
                        {
                            Console.WriteLine("Please enter a valid date");
                        }
                    }
                    else
                    {
                        Console.WriteLine("Can not prase command.");
                    }
                    break;

                case "sensor_type":
                    if (command.Split().Length != 4)
                    {
                        Console.WriteLine("Can not prase command. -set sensortype <channel> <type>");
                        break;
                    }
                    if (!Util.valid_number(command.Split()[2], 1, 8))
                    {
                        Console.WriteLine("Please enter a valid channel between 1 ~ 8");
                        break;
                    }
                    if (!Util.valid_number(command.Split()[3], 0, 5))
                    {
                        Console.WriteLine("Please enter a valid sensor type between 0 ~ 5");
                        Console.WriteLine("0 : Unused");
                        Console.WriteLine("1 : Vibrating Wire");
                        Console.WriteLine("2 : Resistance");
                        Console.WriteLine("3 : Voltage");
                        Console.WriteLine("4 : Frequency");
                        Console.WriteLine("5 : Counter");
                        break;
                    }
                    SendPacket(Packet.SET_Sensor_Type_Packet(request_serial, Int32.Parse(command.Split()[2]) - 1, 
                        Int32.Parse(command.Split()[3])));
                    break;

                case "settle_time":
                    if (command.Split().Length != 4)
                    {
                        Console.WriteLine("Can not prase command. -set settletime <channel> <time>");
                        break;
                    }
                    if (!Util.valid_number(command.Split()[2], 1, 8))
                    {
                        Console.WriteLine("Please enter a valid channel between 1 ~ 8");
                        break;
                    }
                    if (!Util.valid_number(command.Split()[3], 0, 65535))
                    {
                        Console.WriteLine("Please enter a valid settle time between 0 ~ 65535");
                        break;
                    }
                    SendPacket(Packet.SET_VW_Packet(request_serial, (int)Constant.Set_flag.SENESOR_SETTLE, 
                        Int32.Parse(command.Split()[2]) - 1, Int32.Parse(command.Split()[3])));
                    break;

                case "sweep_start":
                    if (command.Split().Length != 4)
                    {
                        Console.WriteLine("Can not prase command. -set sweepstart <channel> <value>");
                        break;
                    }
                    if (!Util.valid_number(command.Split()[2], 1, 8))
                    {
                        Console.WriteLine("Please enter a valid channel between 1 ~ 8");
                        break;
                    }
                    if (!Util.valid_number(command.Split()[3], 0, 65535))
                    {
                        Console.WriteLine("Please enter a valid sweep start between 0 ~ 65535");
                        break;
                    }
                    SendPacket(Packet.SET_VW_Packet(request_serial, (int)Constant.Set_flag.SENSOR_START, 
                        Int32.Parse(command.Split()[2]) - 1, Int32.Parse(command.Split()[3])));
                    break;

                case "sweep_stop":
                    if (command.Split().Length != 4)
                    {
                        Console.WriteLine("Can not prase command. -set sweepstop  <channel> <value>");
                        break;
                    }
                    if (!Util.valid_number(command.Split()[2], 1, 8))
                    {
                        Console.WriteLine("Please enter a valid channel between 1 ~ 8");
                        break;
                    }
                    if (!Util.valid_number(command.Split()[3], 0, 65535))
                    {
                        Console.WriteLine("Please enter a valid sweep stop between 0 ~ 65535");
                        break;
                    }
                    SendPacket(Packet.SET_VW_Packet(request_serial, (int)Constant.Set_flag.SENSOR_STOP, 
                        Int32.Parse(command.Split()[2]) - 1, Int32.Parse(command.Split()[3])));
                    break;

                case "cycles":
                    if (command.Split().Length != 4)
                    {
                        Console.WriteLine("Can not prase command. -set cycles <channel> <value>");
                        break;
                    }
                    if (!Util.valid_number(command.Split()[2], 1, 8))
                    {
                        Console.WriteLine("Please enter a valid channel between 1 ~ 8");
                        break;
                    }
                    if (!Util.valid_number(command.Split()[3], 0, 65535))
                    {
                        Console.WriteLine("Please enter a valid cycles between 0 ~ 65535");
                        break;
                    }
                    SendPacket(Packet.SET_VW_Packet(request_serial, (int)Constant.Set_flag.SENSOR_CYCLES, 
                        Int32.Parse(command.Split()[2]) - 1, Int32.Parse(command.Split()[3])));
                    break;

                case "equation":
                    if (command.Split().Length != 4)
                    {
                        Console.WriteLine("Can not prase command. -set sensorequation <channel> <equation>");
                        break;
                    }
                    if (!Util.valid_number(command.Split()[2], 1, 8))
                    {
                        Console.WriteLine("Please enter a valid channel between 1 ~ 8");
                        break;
                    }
                    if (!Util.valid_number(command.Split()[3], 0, 2))
                    {
                        Console.WriteLine("Please enter a valid sensor type between 0 ~ 2");
                        Console.WriteLine("0 : No Equation, Raw Data");
                        Console.WriteLine("1 : Polynomial");
                        Console.WriteLine("2 : SteinhartHart");
                        break;
                    }
                    SendPacket(Packet.SET_Sensor_Equation_Packet(request_serial, Int32.Parse(command.Split()[2]) - 1, 
                        Int32.Parse(command.Split()[3])));
                    break;

                case "sensor_unit":
                    if (command.Split().Length != 4)
                    {
                        Console.WriteLine("Can not prase command. -set sensorequation <channel> <equation>");
                        break;
                    }
                    if (!Util.valid_number(command.Split()[2], 1, 8))
                    {
                        Console.WriteLine("Please enter a valid channel between 1 ~ 8");
                        break;
                    }
                    SendPacket(Packet.SET_Unit_Packet(request_serial, Int32.Parse(command.Split()[2]) - 1,
                        command.Split()[3].PadRight(4, ' ')));
                    break;

                case "coefficient":
                    if (command.Split().Length != 5)
                    {
                        Console.WriteLine("Can not prase command. -set sensorcof <channel> <index> <coefficient>");
                        break;
                    }
                    if (!Util.valid_number(command.Split()[2], 1, 8))
                    {
                        Console.WriteLine("Please enter a valid channel between 1 ~ 8");
                        break;
                    }
                    if (!Util.valid_number(command.Split()[3], 0, 4))
                    {
                        Console.WriteLine("Please enter a valid index between 0 ~ 4");
                        break;
                    }
                    SendPacket(Packet.SET_Sensor_COF_Packet(request_serial, Int32.Parse(command.Split()[2]) - 1, 
                        Int32.Parse(command.Split()[3]), float.Parse(command.Split()[4])));
                    break;

                case "interval":
                    if (command.Split().Length != 3)
                    {
                        Console.WriteLine("Can not prase command. -set interval <value>");
                        break;
                    }
                    if (!Util.valid_number(command.Split()[2], 2, 65535))
                    {
                        Console.WriteLine("Please enter a valid interval between 2 ~ 65535");
                        break;
                    }
                    SendPacket(Packet.SET_Interval_Packet(request_serial, Int32.Parse(command.Split()[2])));
                    break;

                case "compensated":
                    if (command.Split().Length != 4)
                    {
                        Console.WriteLine("Can not prase command. -set compensated <channel> <enable>");
                        break;
                    }
                    if (!Util.valid_number(command.Split()[2], 1, 8))
                    {
                        Console.WriteLine("Please enter a valid channel between 1 ~ 8");
                        break;
                    }
                    if (!Util.valid_number(command.Split()[3], 0, 1))
                    {
                        Console.WriteLine("Please enter 0 for disable, 1 for enable");
                        break;
                    }
                    SendPacket(Packet.SET_Compensated_Packet(request_serial, Int32.Parse(command.Split()[2]) - 1,
                        Int32.Parse(command.Split()[3])));
                    break;

                case "compensation":
                    if (command.Split().Length != 4)
                    {
                        Console.WriteLine("Can not prase command. -set compensation <channel> <source>");
                        break;
                    }
                    if (!Util.valid_number(command.Split()[2], 1, 8))
                    {
                        Console.WriteLine("Please enter a valid channel between 1 ~ 8");
                        break;
                    }
                    if (!Util.valid_number(command.Split()[3], 1, 8))
                    {
                        Console.WriteLine("Please enter a source channel between 1 ~ 8");
                        break;
                    }
                    SendPacket(Packet.SET_Compensation_Packet(request_serial, Int32.Parse(command.Split()[2]) - 1,
                        Int32.Parse(command.Split()[3]) - 1));
                    break;

                case "mode":
                    if (command.Split().Length != 3)
                    {
                        Console.WriteLine("Can not prase command. -set mode <option>");
                        break;
                    }
                    if (!Util.valid_number(command.Split()[2], 0, 2))
                    {
                        Console.WriteLine("Please enter a valid mode between 0 ~ 2");
                        Console.WriteLine("0 : USB");
                        Console.WriteLine("1 : RS232");
                        Console.WriteLine("2 : RS485");
                        break;
                    }
                    SendPacket(Packet.SET_MODE_Packet(request_serial, Int32.Parse(command.Split()[2])));
                    break;

                case "role":
                    if (command.Split().Length != 3)
                    {
                        Console.WriteLine("Can not prase command. -set role <option> ");
                        break;
                    }
                    if (!Util.valid_number(command.Split()[2], 0, 1))
                    {
                        Console.WriteLine("Please enter a valid role between 0 ~ 1");
                        Console.WriteLine("0 : Master");
                        Console.WriteLine("1 : Slave");
                        break;
                    }
                    SendPacket(Packet.SET_Role_Packet(request_serial, Int32.Parse(command.Split()[2])));
                    break;

                case "reset":
                    if (command.Split().Length != 3)
                    {
                        Console.WriteLine("Can not prase command. -set reset <option> ");
                        break;
                    }

                    switch (command.Split()[2])
                    {
                        case "sensor":
                            SendPacket(Packet.SET_RESET_Packet(request_serial));
                            break;
                        case "sd":
                            SendPacket(Packet.SET_RESETSD_Packet(request_serial));
                            break;
                        default:
                            Console.WriteLine("sensor / sd");
                            break;
                    }
                    break;

                case "counter_reset":
                    if (!Util.valid_number(command.Split()[2], 1, 8))
                    {
                        Console.WriteLine("Please enter a valid channel between 1 ~ 8");
                        break;
                    }
                    if (!Util.valid_number(command.Split()[3], 0, 1))
                    {
                        Console.WriteLine("Please enter 0 for no reset, 1 for reset");
                        break;
                    }
                    SendPacket(Packet.SET_Counter_Reset_Packet(request_serial, 
                        Int32.Parse(command.Split()[2]) - 1, Int32.Parse(command.Split()[3])));
                    break;
                case "record_change":
                    if (!Util.valid_number(command.Split()[2], 1, 8))
                    {
                        Console.WriteLine("Please enter a valid channel between 1 ~ 8");
                        break;
                    }
                    SendPacket(Packet.SET_Sample_Change_Packet(request_serial,
                       Int32.Parse(command.Split()[2]) - 1, float.Parse(command.Split()[3])));
                    break;
                case "slave_list":
                    if (command.Split().Length != 4)
                    {
                        Console.WriteLine("Can not prase command. -set slave_list <slave_number> <serial_number>");
                        break;
                    }
                    if (!Util.valid_number(command.Split()[2], 1, 5))
                    {
                        Console.WriteLine("Please enter a valid slave number between 1 ~ 5");
                        break;
                    }
                    if (!Util.valid_number(command.Split()[3], 0, 65535))
                    {
                        Console.WriteLine("Please enter a valid serial number between 0 ~ 65535");
                        break;
                    }
                    SendPacket(Packet.SET_Slave_List_Packet(request_serial, Int32.Parse(command.Split()[2]) - 1, Int32.Parse(command.Split()[3])));
                    break;
                case "slave_number":
                    if (!Util.valid_number(command.Split()[2], 1, 5))
                    {
                        Console.WriteLine("Please enter a valid slave number between 1 ~ 5");
                        break;
                    }
                    SendPacket(Packet.SET_Slave_Number_Packet(request_serial, Int32.Parse(command.Split()[2])));
                    break;
                default:
                    Menu.Setting_Help();
                    break;
            }
            return false;
        }

        private void Get_data(String command)
        {
            switch (command.Split()[1])
            {
                case "raw_data":
                    if (command.Split().Length == 5)
                    {
                        if (Util.valid_number(command.Split()[2], 0, 9999) && Util.valid_number(command.Split()[3], 0, 12) &&
                            Util.valid_number(command.Split()[4], 0, 31))
                        {
                            _serialPort.DiscardInBuffer();
                            SendPacket(Packet.DAT_RED_Packet(request_serial, Int32.Parse(command.Split()[2]), Int32.Parse(command.Split()[3]), Int32.Parse(command.Split()[4])));
                            Receive_RAW_LOG(Int32.Parse(command.Split()[2]), Int32.Parse(command.Split()[3]), Int32.Parse(command.Split()[4]));
                        }
                        else
                        {
                            Console.WriteLine("Please enter a valid date.");
                        }
                    }
                    else
                    {
                        Console.WriteLine("Can not prase command. -get date <year> <month> <day>");
                    }
                    break;

                case "real_data":
                    if (command.Split().Length == 5)
                    {
                        if (Util.valid_number(command.Split()[2], 0, 9999) && Util.valid_number(command.Split()[3], 0, 12) &&
                            Util.valid_number(command.Split()[4], 0, 31))
                        {
                            _serialPort.DiscardInBuffer();
                            for (int i = 0; i < 8; i++)
                            {
                                if (got_channel[i] == 0)
                                {
                                    logger_info.sensor_info[i] = Get_SensorInfo(i);
                                    got_channel[i] = 1;
                                }
                                Console.Write("Getting sensors info...\r{0}/8   ", i + 1);
                            }
                            Console.WriteLine("");
                            SendPacket(Packet.DAT_RED_Packet(request_serial, Int32.Parse(command.Split()[2]), Int32.Parse(command.Split()[3]), Int32.Parse(command.Split()[4])));
                            Receive_REAL_LOG(Int32.Parse(command.Split()[2]), Int32.Parse(command.Split()[3]), Int32.Parse(command.Split()[4]));
                        }
                        else
                        {
                            Console.WriteLine("Please enter a valid date.");
                        }
                    }
                    else
                    {
                        Console.WriteLine("Can not prase command. -get date <year> <month> <day>");
                    }
                    break;

                case "menu":
                    _serialPort.DiscardInBuffer();
                    SendPacket(Packet.DAT_LST_Packet(request_serial));
                    Receive_MENU();
                    break;

                case "sensor_info":
                    if (command.Split().Length != 3)
                    {
                        Console.WriteLine("Can not prase command. -get sensor <channel>");
                        break;
                    }
                    if (!Util.valid_number(command.Split()[2], 1, 8))
                    {
                        Console.WriteLine("Please enter a valid channel between 1 ~ 8");
                        break;
                    }
                    Constant.Sensor_Info si = Get_SensorInfo(Int32.Parse(command.Split()[2]) - 1);
                    logger_info.sensor_info[Int32.Parse(command.Split()[2]) - 1] = si;
                    got_channel[Int32.Parse(command.Split()[2]) - 1] = 1;
                    Print_SensorInfo(Int32.Parse(command.Split()[2]) - 1);
                    break;
                case "slave_list":
                    _serialPort.DiscardInBuffer();
                    SendPacket(Packet.DAT_SLST_Packet(request_serial));
                    Receive_SLST();
                    break;
                default:
                    Menu.Get_Help();
                    break;
            }
        }

        private void Receive_RAW_LOG(int year, int month, int date)
        {
            if (check_start())
            {
                if (_serialPort.ReadByte() == (int)Constant.Serial_flag.DAT)
                {
                    Directory.CreateDirectory("Data");
                    using (StreamWriter file = new StreamWriter(Environment.CurrentDirectory + string.Format("/Data/{0}_{1}-{2}-{3}_raw.txt", logger_info.serial_num, year, month, date)))
                    {
                        int count = _serialPort.ReadByte() + _serialPort.ReadByte() * 256;
                        int download_count = 1;

                        file.WriteLine("Raw Data | Date: {0}/{1}/{2} | Total number of records: {3}", date, month, year, count);
                        file.WriteLine("                      Channel 1   Channel 2    Channel 3    Channel 4    Channel 5    Channel 6    Channel 7    Channel 8");

                        while (true)
                        {
                            int year1 = _serialPort.ReadByte();
                            int year2 = _serialPort.ReadByte();
                            if (year1 == Constant.STOP_BYTE && year2 == Constant.STOP_BYTE)
                            {
                                break;
                            }
                            Console.Write("\rDownloading...{0}%   ", download_count * 100 / count);
                            file.Write("{0:0000}/", Util.uin8_to_uin16(year1, year2));
                            file.Write("{0:00}/{1:00} ", _serialPort.ReadByte(), _serialPort.ReadByte());
                            file.Write("{0:00}:{1:00}:{2:00} ", _serialPort.ReadByte(), _serialPort.ReadByte(), _serialPort.ReadByte());
                            _serialPort.ReadByte();
                            for (int i = 0; i < 8; i++)
                            {
                                byte[] floatarray = new byte[4];
                                floatarray[0] = (byte)_serialPort.ReadByte();
                                floatarray[1] = (byte)_serialPort.ReadByte();
                                floatarray[2] = (byte)_serialPort.ReadByte();
                                floatarray[3] = (byte)_serialPort.ReadByte();
                                if (BitConverter.ToSingle(floatarray, 0) != 0)
                                {
                                    file.Write(" {0: 00000.000;-00000.000} | ", Math.Round(BitConverter.ToSingle(floatarray, 0), 3));
                                }
                                else
                                {
                                    file.Write("           | ");
                                }
                            }
                            file.WriteLine();
                            download_count++;
                        }
                    }
                    Console.WriteLine("DownLoad Done");
                }
                else
                {
                    Console.WriteLine("Date not available");
                }
            }
            else
            {
                Console.WriteLine("Date Not Found");
            }
        }

        private void Receive_REAL_LOG(int year, int month, int date)
        {
            if (check_start())
            {
                if (_serialPort.ReadByte() == (int)Constant.Serial_flag.DAT)
                {
                    Directory.CreateDirectory("Data");
                    using (StreamWriter file = new StreamWriter(Environment.CurrentDirectory + string.Format("/Data/{0}_{1}-{2}-{3}_real.txt", logger_info.serial_num, year, month, date)))
                    {
                        for (int i = 0; i < 8; i++)
                        {
                            file.Write(String.Format("Channel {0}", i + 1).PadRight(25, ' '));
                        }
                        file.Write(System.Environment.NewLine);
                        for (int i = 0; i < 8; i++)
                        {
                            file.Write(String.Format("Type: {0}", Constant.sensor_types[logger_info.sensor_info[i].type]).PadRight(25, ' '));
                        }
                        file.Write(System.Environment.NewLine);
                        for (int i = 0; i < 8; i++)
                        {
                            file.Write(String.Format("Unit: {0}", logger_info.sensor_info[i].unit).PadRight(25, ' '));
                        }
                        file.Write(System.Environment.NewLine);
                        for (int i = 0; i < 8; i++)
                        {
                            file.Write(String.Format("Settle Time(ms): {0}", logger_info.sensor_info[i].settle).PadRight(25, ' '));
                        }
                        file.Write(System.Environment.NewLine);
                        for (int i = 0; i < 8; i++)
                        {
                            file.Write(String.Format("Sweep Start(Hz): {0}", logger_info.sensor_info[i].excite_start).PadRight(25, ' '));
                        }
                        file.Write(System.Environment.NewLine);
                        for (int i = 0; i < 8; i++)
                        {
                            file.Write(String.Format("Sweep Stop(Hz): {0}", logger_info.sensor_info[i].excite_stop).PadRight(25, ' '));
                        }
                        file.Write(System.Environment.NewLine);
                        for (int i = 0; i < 8; i++)
                        {
                            file.Write(String.Format("Cycles: {0}", logger_info.sensor_info[i].cycle).PadRight(25, ' '));
                        }
                        file.Write(System.Environment.NewLine);
                        for (int i = 0; i < 8; i++)
                        {
                            file.Write(String.Format("Equation: {0}", Constant.equation_types[logger_info.sensor_info[i].equation]).PadRight(25, ' '));
                        }
                        file.Write(System.Environment.NewLine);
                        for (int i = 0; i < 8; i++)
                        {
                            file.Write(String.Format("Coef x^0: {0}", logger_info.sensor_info[i].cof[0]).PadRight(25, ' '));
                        }
                        file.Write(System.Environment.NewLine);
                        for (int i = 0; i < 8; i++)
                        {
                            file.Write(String.Format("Coef x^1: {0}", logger_info.sensor_info[i].cof[1]).PadRight(25, ' '));
                        }
                        file.Write(System.Environment.NewLine);
                        for (int i = 0; i < 8; i++)
                        {
                            file.Write(String.Format("Coef x^2: {0}", logger_info.sensor_info[i].cof[2]).PadRight(25, ' '));
                        }
                        file.Write(System.Environment.NewLine);
                        for (int i = 0; i < 8; i++)
                        {
                            file.Write(String.Format("Coef x^3: {0}", logger_info.sensor_info[i].cof[3]).PadRight(25, ' '));
                        }
                        file.Write(System.Environment.NewLine);
                        for (int i = 0; i < 8; i++)
                        {
                            file.Write(String.Format("Coef x^4: {0}", logger_info.sensor_info[i].cof[4]).PadRight(25, ' '));
                        }
                        file.Write(System.Environment.NewLine);

                        int count = _serialPort.ReadByte() + _serialPort.ReadByte() * 256;
                        int download_count = 0;

                        file.Write(System.Environment.NewLine);
                        file.WriteLine("Real Data | Date: {0}/{1}/{2} | Total number of records: {3}", date, month, year, count);
                        file.WriteLine("                      Channel 1   Channel 2    Channel 3    Channel 4    Channel 5    Channel 6    Channel 7    Channel 8");

                        Constant.Record[] records = new Constant.Record[count];

                        while (true)
                        {
                            int year1 = _serialPort.ReadByte();
                            int year2 = _serialPort.ReadByte();
                            if (year1 == Constant.STOP_BYTE && year2 == Constant.STOP_BYTE)
                            {
                                break;
                            }
                            int month1 = _serialPort.ReadByte();
                            int date1 = _serialPort.ReadByte();
                            int hour1 = _serialPort.ReadByte();
                            int minute1 = _serialPort.ReadByte();
                            int second1 = _serialPort.ReadByte();
                            _serialPort.ReadByte();
                            float[] r = new float[8];
                            for (int i = 0; i < 8; i++)
                            {
                                byte[] floatarray = new byte[4];
                                floatarray[0] = (byte)_serialPort.ReadByte();
                                floatarray[1] = (byte)_serialPort.ReadByte();
                                floatarray[2] = (byte)_serialPort.ReadByte();
                                floatarray[3] = (byte)_serialPort.ReadByte();
                                r[i] = BitConverter.ToSingle(floatarray, 0);
                            }
                            records[download_count] = new Constant.Record(new DateTime(Util.uin8_to_uin16(year1, year2), month1, date1, hour1, minute1, second1), r);
                            Console.Write("\rDownloading...{0}%   ", (download_count + 1) * 100 / count);
                            download_count++;
                        }

                        for (int i = 0; i < count; i++)
                        {
                            file.Write(records[i].t.ToString("dd/MM/yyyy HH:mm:ss "));
                            float[] real = calculate_real_reading(records[i].readings);
                            for (int j = 0; j < 8; j++)
                            {
                                if (real[j] != 0)
                                {
                                    file.Write(" {0: 00000.000;-00000.000} | ", Math.Round(real[j], 3));
                                }
                                else
                                {
                                    file.Write("           | ");
                                }
                            }
                            file.WriteLine();
                        }
                    }
                    Console.WriteLine("DownLoad Done");
                }
                else
                {
                    Console.WriteLine("Date not available");
                }
            }
            else
            {
                Console.WriteLine("Date Not Found");
            }
        }

        private void Receive_MENU()
        {
            if (check_start())
            {
                if (_serialPort.ReadByte() == (int)Constant.Serial_flag.DAT)
                {
                    while (true)
                    {
                        int year1 = _serialPort.ReadByte();
                        int year2 = _serialPort.ReadByte();
                        if (year1 == Constant.STOP_BYTE && year2 == Constant.STOP_BYTE)
                        {
                            break;
                        }
                        Console.Write("{0}/", Util.uin8_to_uin16(year1, year2));
                        Console.Write("{0}/{1} ", _serialPort.ReadByte(), _serialPort.ReadByte());
                        Console.Write("\n");
                    }
                }
            }
        }

        private void Receive_SLST()
        {
            if (check_start())
            {
                int flag = _serialPort.ReadByte();
                if (flag == (int)Constant.Serial_flag.DAT)
                {
                    for (int i = 0; i < 5; i++)
                    {
                        int serial1 = _serialPort.ReadByte();
                        int serial2 = _serialPort.ReadByte();
                        if (Util.uin8_to_uin16(serial1, serial2) != 0)
                        {
                            Console.WriteLine("Slave {0} : {1}", i + 1, Util.uin8_to_uin16(serial1, serial2));
                        }
                        else
                        {
                            Console.WriteLine("Slave {0} : No", i + 1);
                        }
                        logger_info.slave_list[i] = Util.uin8_to_uin16(serial1, serial2);
                    }
                }
                else
                {
                    Console.WriteLine("Connected decive not a Master");
                    Console.WriteLine("Slave Number : {0}", flag + 1);
                }
            }
        }

        private void RTM_mode()
        {
            Console.WriteLine("Press ESC to exit Real Time Measure Mode");
            
            Thread rtmThread = new Thread(RTMthread);
            rtmThread.Start();

            do
            {
                while (!Console.KeyAvailable)
                {
                    Thread.Sleep(100);
                }
            } while (Console.ReadKey(true).Key != ConsoleKey.Escape);

            SendPacket(Packet.DAT_RTM_Packet(request_serial, 0));
            rtmThread.Abort();
            Console.WriteLine("Exit Real Time Mode");
        }

        private void RTMthread()
        {
            while (true)
            {
                for (int i = 0; i < 8; i++)
                {
                    int[] data = get_packet();
                    while (data == null)
                    {
                        data = get_packet();
                    }
                    if (data[0] == (int)Constant.Serial_flag.DAT)
                    {
                        float reading = Util.int_to_float(data[2], data[3], data[4], data[5]);

                        Console.Write(" {0:00000.000} | ", Math.Round(reading, 3));
                    }
                }
                Console.Write("\n");
            }
        }

        private bool Start_handshake()
        {
            _serialPort.DiscardInBuffer();
            _serialPort.DiscardOutBuffer();

            if(!SendPacket(Packet.SYN_Packet(request_serial)))
            {
                Console.WriteLine("Sending SYN Packet Failed");
                return false;
            }

            int[] packet = get_packet();

            if (packet != null)
            {
                if (packet[0] == (int)Constant.Serial_flag.ACK_SYN)
                {
                    logger_info.serial_num = Util.uin8_to_uin16(packet[1], packet[2]);
                    logger_info.fmw_num = Util.uin8_to_uin16(packet[3], packet[4]);

                    Console.WriteLine(String.Format("Successfully connected to Logger on {0}", _portName));
                    Console.WriteLine("/------------------------------");
                    Console.WriteLine(String.Format("Serial Number: {0}", logger_info.serial_num));
                    Console.WriteLine(String.Format("Firmware Number: {0}", logger_info.fmw_num));

                    try
                    {
                        logger_info.last_set = new DateTime(Util.uin8_to_uin16(packet[5], packet[6]), packet[7], packet[8], packet[9], packet[10], packet[11]);
                        Console.WriteLine(String.Format("Last Setting Date: {0}", logger_info.last_set));
                    }
                    catch (Exception)
                    {
                        Console.WriteLine("Last Setting Date not proper");
                    }

                    try
                    {
                        logger_info.build_date = new DateTime(Util.uin8_to_uin16(packet[12], packet[13]), packet[14], packet[15], packet[16], packet[17], packet[18]);
                        Console.WriteLine(String.Format("Build Date: {0}", logger_info.last_set));
                    }
                    catch (Exception)
                    {
                        Console.WriteLine("Build Date not proper");
                    }

                    try
                    {
                        logger_info.check_point = new DateTime(Util.uin8_to_uin16(packet[19], packet[20]), packet[21], packet[22], packet[23], packet[24], packet[25]);
                        Console.WriteLine(String.Format("Check Point Date: {0}", logger_info.last_set));
                    }
                    catch (Exception)
                    {
                        Console.WriteLine("Check Point Date not proper");
                    }

                    logger_info.sensor_info = new Constant.Sensor_Info[8];

                    Console.WriteLine("");
                    for (int i = 0; i < 8; i++)
                    {
                        logger_info.sensor_info[i].type = packet[26 + i];
                        Console.WriteLine("Sensor {0} : {1}", i + 1, Constant.sensor_types[packet[26 + i]]);
                    }

                    logger_info.role = packet[34];
                    logger_info.slave_list = new int[5];
                    Console.WriteLine("");
                    if (packet[34] == 0)
                    {
                        Console.WriteLine("Role: Master");
                        for (int i = 0; i < 5; i++)
                        {
                            logger_info.slave_list[i] = Util.uin8_to_uin16(packet[35 + i * 2], packet[36 + i * 2]);
                            Console.WriteLine("Slave {0} : {1}", i + 1, logger_info.slave_list[i]);
                        }
                    }

                    else
                    {
                        Console.WriteLine("Role: Slave");
                        Console.WriteLine("Slave Number : {0}", packet[45] + 1);
                    }

                    Console.WriteLine("/------------------------------");
                }
                else
                {
                    Console.WriteLine(String.Format("Fail to HandSake with Logger"));
                    _serialPort.DiscardInBuffer();
                    return false;
                }
            }
            else
            {
                Console.WriteLine(String.Format("Fail to HandSake with Logger"));
                _serialPort.DiscardInBuffer();
                return false;
            }

            if (!SendPacket(Packet.ACK_SYN_Packet(request_serial)))
            {
                Console.WriteLine("Sending ACK_SYN Packet Failed");
                return false;
            }
            return true;
        }

        public Constant.Sensor_Info Get_SensorInfo(int channel)
        {
            Constant.Sensor_Info info = new Constant.Sensor_Info();
            info.cof = new float[5];
            _serialPort.DiscardInBuffer();
            _serialPort.DiscardOutBuffer();

            if (!SendPacket(Packet.DAT_SensorInfo_Packet(request_serial, channel)))
            {
                Console.WriteLine("Sending SensorInfo Failed");
            }
            else
            {
                int[] packet = get_packet();

                if (packet != null)
                {
                    if (packet[0] == (int)Constant.Serial_flag.DAT)
                    {
                        info.type = packet[1];
                        info.settle = Util.uin8_to_uin16(packet[2], packet[3]);
                        info.excite_start = Util.uin8_to_uin16(packet[4], packet[5]);
                        info.excite_stop = Util.uin8_to_uin16(packet[6], packet[7]);
                        info.cycle = Util.uin8_to_uin16(packet[8], packet[9]);
                        info.equation = packet[10];
                        for (int i = 0; i < 5; i++)
                        {
                            info.cof[i] = Util.int_to_float(packet[11 + i * 4],
                                packet[12 + i * 4], packet[13 + i * 4], packet[14 + i * 4]);
                        }
                        info.compensation = packet[31];
                        info.record_change = Util.int_to_float(packet[32],
                                packet[33], packet[34], packet[35]);
                        info.counter_reset = (packet[36] & (1 << channel)) > 0 ? 1 : 0;
                        info.unit = String.Format("{0}{1}{2}{3}", (char)packet[37], (char)packet[38], (char)packet[39], (char)packet[40]);

                        return info;

                    }
                }
            }

            return info;
        }

        private void Print_SensorInfo(int channel)
        {
            Console.WriteLine("/------------------------");
            Console.WriteLine(String.Format("Sensor info for channel {0}", channel + 1));
            Console.WriteLine(String.Format("Type: {0}", Constant.sensor_types[logger_info.sensor_info[channel].type]));
            Console.WriteLine(String.Format("Unit: {0}", logger_info.sensor_info[channel].unit));
            Console.WriteLine(String.Format("Settle Time(ms): {0}", logger_info.sensor_info[channel].settle));
            Console.WriteLine(String.Format("Sweep Start(Hz): {0}", logger_info.sensor_info[channel].excite_start));
            Console.WriteLine(String.Format("Sweep Stop(Hz): {0}", logger_info.sensor_info[channel].excite_stop));
            Console.WriteLine(String.Format("Cycles: {0}", logger_info.sensor_info[channel].cycle));
            Console.WriteLine(String.Format("Equation: {0}", Constant.equation_types[logger_info.sensor_info[channel].equation]));
            Console.WriteLine(String.Format("Coefficient x^0: {0}", logger_info.sensor_info[channel].cof[0]));
            Console.WriteLine(String.Format("Coefficient x^1: {0}", logger_info.sensor_info[channel].cof[1]));
            Console.WriteLine(String.Format("Coefficient x^2: {0}", logger_info.sensor_info[channel].cof[2]));
            Console.WriteLine(String.Format("Coefficient x^3: {0}", logger_info.sensor_info[channel].cof[3]));
            Console.WriteLine(String.Format("Coefficient x^4: {0}", logger_info.sensor_info[channel].cof[4]));
            if (logger_info.sensor_info[channel].compensation == 0)
            {
                Console.WriteLine("Compensation: No");
            }
            else
            {
                Console.WriteLine("Compensation: {0}" , logger_info.sensor_info[channel].compensation);
            }

            if (logger_info.sensor_info[channel].record_change == 0)
            {
                Console.WriteLine("Record On Change: No");
            }
            else
            {
                Console.WriteLine(String.Format("Record On Change: {0}", logger_info.sensor_info[channel].record_change));
            }
            if (logger_info.sensor_info[channel].type == 5)
            {
                Console.WriteLine(String.Format("Counter Reset: {0}", logger_info.sensor_info[channel].counter_reset == 0 ? "No" : "Yes"));
            }
            Console.WriteLine("/------------------------");
        }

        private float[] calculate_real_reading(float[] raw_reading)
        {
            float[] real = new float[8];
            for (int i = 0; i < 8; i++)
            {
                if (logger_info.sensor_info[i].compensation == 0)
                {
                    switch (logger_info.sensor_info[i].equation)
                    {
                        case 0:
                            real[i] = raw_reading[i];
                            break;
                        case 1:
                            real[i] = Calculation.Polynomial(logger_info.sensor_info[i].cof, raw_reading[i], 0);
                            break;
                        case 2:
                            real[i] = (float)Calculation.Steinhh(logger_info.sensor_info[i].cof, raw_reading[i], 0);
                            break;
                    }
                }
            }

            for (int i = 0; i < 8; i++)
            {
                if (logger_info.sensor_info[i].compensation != 0)
                {
                    switch (logger_info.sensor_info[i].equation)
                    {
                        case 0:
                            real[i] = raw_reading[i];
                            break;
                        case 1:
                            real[i] = Calculation.Polynomial(logger_info.sensor_info[i].cof, raw_reading[i], real[logger_info.sensor_info[i].compensation - 1]);
                            break;
                        case 2:
                            real[i] = (float)Calculation.Steinhh(logger_info.sensor_info[i].cof, raw_reading[i], real[logger_info.sensor_info[i].compensation - 1]);
                            break;
                    }
                }
            }

            return real;
        }

        private void Disconnect()
        {
            _serialPort.Close();
            Console.WriteLine("Serial Port Closed, disconnected from current logger");
        }

        // Get the leatest packet start with STARTBIT end with STOPBIT
        private int[] get_packet()
        {
            int[] current_packet = new int[80];
            
            int write_pos = 0;

            if (!check_start())
            {
                return null;
            }

            int current_byte = _serialPort.ReadByte();
            int next_byte = 0;
            
            while (true)
            {
                current_packet[write_pos] = current_byte;
                write_pos++;

                if (next_byte != 0)
                {
                    current_packet[write_pos] = next_byte;
                    write_pos++;
                    next_byte = 0;
                }

                current_byte = _serialPort.ReadByte();

                if (current_byte == Constant.STOP_BYTE)
                {
                    next_byte = _serialPort.ReadByte();
                    
                    if (next_byte == Constant.STOP_BYTE)
                    {
                        break;
                    }

                }
            }

            return current_packet;
        }

        // Send a packet to logger if conneted, return true if success
        private bool SendPacket(Constant.Packet_Info info)
        {
            if (_serialPort != null)
            {
                try
                {
                    _serialPort.Write(Constant.START_BYTE_ARY, 0, 1);
                    _serialPort.Write(info.data, info.offset, info.count);
                    _serialPort.Write(Constant.STOP_BYTE_ARY, 0, 1);
                }
                catch (Exception)
                {
                    return false;
                }
                return true;
            }
            else
            {
                Console.WriteLine("No Connected Port!");
                return false;
            }
        }

        private bool check_start()
        {
            int timer = 0;

            do
            {
                Thread.Sleep(5);

                timer++;
                if (timer > 2000)
                {
                    Console.WriteLine("Receive Packet Time Out");
                    return false;
                }
            } while (_serialPort.BytesToRead == 0);

            do
            {
                Thread.Sleep(5);

                timer++;
                if (timer > 2000)
                {
                    Console.WriteLine("Receive Packet Time Out");
                    return false;
                }
            } while (_serialPort.ReadByte() != Constant.START_BYTE);

            do
            {
                Thread.Sleep(5);

                timer++;
                if (timer > 2000)
                {
                    Console.WriteLine("Receive Packet Time Out");
                    return false;
                }
            } while (_serialPort.ReadByte() != Constant.START_BYTE);

            return true;
        }
    }
}
