﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Logger_Console
{
    class Menu
    {
        public static void Intro()
        {
            Console.WriteLine("Sigra Logger Console Interface");
            Console.WriteLine();
            Console.WriteLine("Enter command to interact, enter -h to get more info");
        }
        public static void Main_Help()
        {
            Console.WriteLine("/------------------------");
            Console.WriteLine("-rescan                      Rescan and show all connected COM ports");
            Console.WriteLine("-connect <portname> <Mode>   Connect to a specific port, the port name should be the same as -ap printed. For Mode, 0 for USB, 1 for Modem");
            Console.WriteLine("-h                           Show command dictionary for main menu");
            Console.WriteLine("/------------------------");
        }
        public static void Connected_Help()
        {
            Console.WriteLine("/------------------------");
            Console.WriteLine("-disconnect              Disconnect to device");
            Console.WriteLine("-h                       Print all possible commands");
            Console.WriteLine("-rtm                     Enable Real Time Reading");
            Console.WriteLine("-set <setting> <value>   Push setting to Logger, enter -sethelp to get more info about setting");
            Console.WriteLine("-get <type> <parameter>  Get data from logger, enter -gethelp to get more info");
            Console.WriteLine("-seth                    Print Command list for push settings");
            Console.WriteLine("-geth                    Print Command list for get command");
            Console.WriteLine("-clear                   Clear the console");
            Console.WriteLine("/------------------------");
        }

        public static void Setting_Help()
        {
            Console.WriteLine("/------------------------");
            Console.WriteLine("serial_number <serial number>        Set the serial number for the logger");
            Console.WriteLine("firmware_version <firmware version>  Set the firmware version for the logger");
            Console.WriteLine("clock                                Set the time of the logger to local time");
            Console.WriteLine("build_date                           Set the build date for logger");
            Console.WriteLine("check_point                          Set the check point date for logger");
            Console.WriteLine("reset sensor                         Reset the sensor settings to default");
            Console.WriteLine("reset sd                             Reset the sd card to default");
            Console.WriteLine("sensor_type <channel> <type>         Set the type for certain channel, 0 for UNUSED, 1 for VW, 2 for resis, 3 for volt");
            Console.WriteLine("settle_time <channel> <time>         Set the settle for certain channel, unit in ms");
            Console.WriteLine("sweep_start <channel> <value>        Set the exict start");
            Console.WriteLine("sweep_stop <channel> <value>         Set the exict stop");
            Console.WriteLine("cycles <channel> <value>             Set the cycles for certain channel");
            Console.WriteLine("equation <channel> <equation>        Set the equation for certain channel");
            Console.WriteLine("coefficient <channel> <index> <cof>  Set the coefficient for certain channel");
            Console.WriteLine("compensated <channel> <enable>       Enable/disable compensation");
            Console.WriteLine("compensation <channel> <tchannel>    Set the target channel for compensation");
            Console.WriteLine("interval <value>                     Set theinterval between each measure, unit in second");
            Console.WriteLine("mode <option>                        Set the connection mode for logger. 0 for USB, 1 for RS232 2 for RS485");
            Console.WriteLine("role <option>                        Set the role for logger. 0 for master, 1 for slave");
            Console.WriteLine("counter_reset <channel> <option>     Set the role for logger. 0 for master, 1 for slave");
            Console.WriteLine("record_change <channel> <option>     Set the role for logger. 0 for master, 1 for slave");
            Console.WriteLine("slave_list <slave_number> <serial>   Set the slave_list for master logger. Slave Number can only be 1 to 5");
            Console.WriteLine("slave_number <value>                 Set the slave_number for logger, Slave Number can only be 1 to 5");
            Console.WriteLine("/------------------------");
        }

        public static void Get_Help()
        {
            Console.WriteLine("/------------------------");
            Console.WriteLine("sensor_info <channel>                Get the sensor info for certain channel");
            Console.WriteLine("raw_data <year> <month> <day>        Download the logger raw data for that date");
            Console.WriteLine("real_data <year> <month> <day>       Download the logger raw data for that date, and calculate real data using the current sensor settings in logger");
            Console.WriteLine("menu                                 Get all available dates in this logger");
            Console.WriteLine("slave_list                           Get the slave list from master logger, or slave number from slave logger instead.");             
            Console.WriteLine("/------------------------");
        }
    }
}
